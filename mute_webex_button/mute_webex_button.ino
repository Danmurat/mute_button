#include <Keyboard.h>


void setup() {
  pinMode(3,INPUT_PULLUP); 
// Serial.begin(9600);    //begin seral comms for debugging
  pinMode(5, OUTPUT);    // sets the digital pin 5 as output RED
//  pinMode(9, OUTPUT);    // sets the digital pin 9 as output GREEN
}

int onOff = 0;

void loop() {
  
  Keyboard.begin();
  
  if (digitalRead(3) == 0) //if button 3 is pushed
   {
    
    //Mute shortcut on webex = cmd + shift + p
    Keyboard.press(KEY_LEFT_GUI); //Left Shift Key
    Keyboard.press(KEY_LEFT_SHIFT); //Command Key Mac
    Keyboard.press('p'); //p
  
    Keyboard.releaseAll();

    delay(200);

    //Mute shortcut on webex = cmd + shift + m (Specifically for webex)
    Keyboard.press(KEY_LEFT_GUI); //Left Shift Key
    Keyboard.press(KEY_LEFT_SHIFT); //Command Key Mac
    Keyboard.press('m'); //m
  
    Keyboard.releaseAll();
    
    if(onOff==0){
      digitalWrite(5, HIGH); // sets the digital pin 5 on
//      digitalWrite(9, LOW); // sets the digital pin 9 off
      onOff=1;
    }
    else{
//      digitalWrite(9, HIGH); // sets the digital pin 9 on
      digitalWrite(5, LOW); // sets the digital pin 5 off
      onOff=0;
    }

    delay(1200);
    
  }

  Keyboard.end();
}
